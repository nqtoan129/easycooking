//
//  Threads.swift
//  EasyCooking
//
//  Created by ToanNguyen on 8/28/20.
//  Copyright © 2020 ToanNguyen. All rights reserved.
//

import Foundation

extension Thread {
    static func runInMain(execute: @escaping () -> ()) {
        if !Thread.isMainThread {
            DispatchQueue.main.async {
                execute()
            }
        } else {
            execute()
        }
    }
}
